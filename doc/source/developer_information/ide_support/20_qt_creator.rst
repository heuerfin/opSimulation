..
  *******************************************************************************
  Copyright (c) 2021 in-tech GmbH

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _qtcreator:

Working with Qt Creator
=======================

Settings (MSYS2)
----------------

Setup CMake-Kit
~~~~~~~~~~~~~~~

#. Add CMake under ``Tools -> Options -> Kits -> CMake``

   .. figure:: _static/images/qtcreator_settings_cmake.png
      :align: center
      :scale: 60%


#. Add C and C++ Compilers under ``Tools -> Options -> Kits -> Compilers``

   .. figure:: _static/images/qtcreator_settings_g++.png
      :align: center
      :scale: 60%


   .. figure:: _static/images/qtcreator_settings_gcc.png
      :align: center
      :scale: 60%


#. Add Debugger under ``Tools -> Options -> Kits -> Debuggers``

   .. figure:: _static/images/qtcreator_settings_gdb.png
      :align: center
      :scale: 60%


#. Add Kit under ``Tools -> Options -> Kits -> Kits``

   .. figure:: _static/images/qtcreator_settings_kit.png
      :align: center
      :scale: 60%


#. Adjust ``CMake Generator`` under ``Tools -> Options -> Kits -> Kits`` to ``MSYS Makefiles``

   .. figure:: _static/images/qtcreator_settings_cmake_generator.png
      :align: center
      :scale: 60%

   When loading a ``CMakeLists.txt`` doesn't result in listing all relevant files, a change of the ``CMake Generator`` to ``MinGW Makefiles`` with a matching ``Extra generator`` is recommended. 
   This has to be noted while executing the ``Prepare build`` step in :ref:`Installing OpenPASS <download_and_install_openpass>`. 

   .. note::
      
      Change the environment variable to ``MAKEFLAGS=-j4`` (or similar) to enable parallel building on the kit level.

Setup Project
~~~~~~~~~~~~~~

#. Load the project by opening a ``CMakeLists.txt`` file and configure it to use the new kit.

#. Setup missing CMake flags (c.f. :ref:`Cmake`). 
   For building the documentation and also running the end-to-end tests the variable ``Python3_EXECUTABLE`` needs to be set to a native Windows Python environment, which has to include all required libraries, because the Python environment used in MSYS2 will not work here.   

   .. todo::
    
      The configuration is still "itchy", as Qt creator changes the interface with every version
      See https://gitlab.eclipse.org/eclipse/simopenpass/simopenpass/-/issues/149


Settings (Linux)
----------------

Setup Kit
~~~~~~~~~

Qt Creator should be able to come up with a suiteable kit for developing |op| on its own.
If not, check if all :ref:`prerequisites` are met. Also, the steps above should be a good indicator, where to look for potential troubles.

Setup Project
~~~~~~~~~~~~~~

#. Load the project by opening a ``CMakeLists.txt`` file and configure it to use a proper kit.

#. Setup missing CMake flags (c.f. :ref:`Cmake`)

   .. todo::
    
      The configuration is still "itchy", as Qt creator changes the interface with every version
      See https://gitlab.eclipse.org/eclipse/simopenpass/simopenpass/-/issues/149

Alternative Method
------------------

.. admonition:: Version Issues
   
   Qt Creator is constantly improving the CMake integration.
   Unfortunatly, some versions seem to be **buggy**.

   Tested Versions: 4.13.2 | 4.12.2

#. Follow the install instructions to invoke cmake from within your own build directory.
   - Execute the step for ``build-release``
   - Execute the step for ``build-debug``
#. Open Qt Creator
#. Open ``CMakeLists.txt``
#. Qt Creator should find the configured build directories and create a temporary kit. 
   It is recommended to use the the Kit prepared under the step ``Setup CMake-Kit`` and change therefore the paths for the ``Release`` resp. ``Debug`` build from the ``Prepare build`` step in :ref:`Installing OpenPASS <download_and_install_openpass>`. 

   .. figure:: _static/images/qtcreator_project_cmake.png
      :align: center
      :scale: 60%

   .. figure:: _static/images/qtcreator_project_loaded.png
      :align: center
      :scale: 60%

   .. figure:: _static/images/qtcreator_project_cmake_settings.png
      :align: center
      :scale: 60%

   It is possible to add an additional build step which also includes the end-to-end tests. Therefore the CMake variable ``WITH_ENDTOEND_TESTS`` needs to be set. 

   .. figure:: _static/images/qtcreator_adding_build_steps.png
      :align: center
      :scale: 60%

#. After the build, the link to the ``opSimulation.exe`` resp. the ``opSimulationManager.exe`` within the ``Run Settings`` is possible. When using ``Debug`` as the build-type, the Qt-creator should be able to run the simulation with debugging.  

   .. figure:: _static/images/qtcreator_adding_run_configuration.png
      :align: center
      :scale: 60%